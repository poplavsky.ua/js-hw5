/*
1. Опишіть своїми словами, що таке метод об'єкту
    Метод об'єкту - це властивість об'єкта, значенням якого є функція, яка в свою чергу виконує певні дії над даними об'єкта.
2. Який тип даних може мати значення властивості об'єкта?
    const Arr = [1, 2, 3]; // масив
    const obj = {age: 18}; // об'єкт
    const nl = null; // значення "null"
    console.log(Arr, typeof(Arr)); // object
    console.log(obj, typeof(obj)); // object
    console.log(nl, typeof(nl)); // object (узгоджена помилка JavaScript, насправді це не об'єкт)
3. Об'єкт це посилальний тип даних. Що означає це поняття?
    Це означає, що змінна, яка має посилальний тип даних, фактично не містить значення. Вона містить посилання на місце в пам'яті, де розміщуються реальні дані. Тобто робота з даними об'єкта ведеться виключно через посилання.
*/

/*
Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати ім'я та прізвище.
Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності:
Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
*/

function createNewUser () {
  let firstName = prompt("Введіть ім'я");
  let lastName = prompt("Введіть прізвище");
  
  const newUser = {
    _firstName: firstName,
    _lastName: lastName,

    set firstName(value) {
      this._firstName = value;
    },
    get firstName() {
      return this._firstName;
    },
    set lastName(value) {
      this._lastName = value;
    },
    get lastName() {
      return this._lastName;
    },

    getLogin: function() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
  }

  return newUser;
}

let result = createNewUser();
console.log(result);
console.log(result.getLogin());
